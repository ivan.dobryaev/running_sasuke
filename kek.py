import pygame
import sys
gameover = False
size = width, height = 320, 240
screen = None
bg = None

ball_coords_x = 60
ball_coords_y = 50


def handle_events():
    global gameover
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameover = True


def init():
    global screen, bg
    pygame.init()
    bg = (255, 255, 0)
    screen = pygame.display.set_mode(size)

def main():
    global gameover, ball_coords_x, ball_coords_y
    init()
    while not gameover:
        handle_events()
        screen.fill(bg)
        pygame.display.flip()


main()






