def main():
	inp = open("in.txt", "r")
	out = open("out.txt", "w")
	lines = inp.readlines()
	out.write(str(len(lines)) + '\n')
	symb_count = 0
	for line in lines:
		symb_count += len(line)
	out.write(str(symb_count - len(lines) + 1) + '\n')
	if len(lines) >= 3:
		out.write(lines[2])
	else:
		out.write("0")

if __name__ == '__main__':
	main()