import pygame
import sys
import time

gameover = False
size = width, height = 700, 700
screen = None
bg = None

ball_coords_x = 60
ball_coords_y = 50

class trafficLight():
    def __init__(self):
        self.red = 0
        self.yellow = 0
        self.green = 1

    def turn(self):
        if self.green == 1:
            self.green = 0
            self.red = 0
            self.yellow = 1
        elif self.yellow == 1:
            self.yellow = 0
            self.green = 0
            self.red = 1
        elif self.red == 1:
            self.red = 0
            self.yellow = 0
            self.green = 1

    def draw(self):
        global screen
        if self.red == 1:
            pygame.draw.circle(screen, (255, 0, 0), (width // 2, height // 2 - 300), 100, 0)
            pygame.draw.circle(screen, (255, 255, 0), (width // 2, height // 2), 100, 5)
            pygame.draw.circle(screen, (0, 255, 0), (width // 2, height // 2 + 300), 100, 5)
        elif self.yellow == 1:
            pygame.draw.circle(screen, (255, 0, 0), (width // 2, height // 2 - 300), 100, 5)
            pygame.draw.circle(screen, (255, 255, 0), (width // 2, height // 2), 100, 0)
            pygame.draw.circle(screen, (0, 255, 0), (width // 2, height // 2 + 300), 100, 5)
        elif self.green == 1:
            pygame.draw.circle(screen, (255, 0, 0), (width // 2, height // 2 - 300), 100, 5)
            pygame.draw.circle(screen, (255, 255, 0), (width // 2, height // 2), 100, 5)
            pygame.draw.circle(screen, (0, 255, 0), (width // 2, height // 2 + 300), 100, 0)



def handle_events():
    global gameover
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameover = True


def init():
    global screen, bg
    pygame.init()
    bg = (255, 255, 255)
    screen = pygame.display.set_mode(size)

def main():
    ball = pygame.image.load("ball.jpg")
    ball = pygame.transform.scale(ball, (250,250))
    ballrect = ball.get_rect()
    ballrect.x = 100
    ballrect.y = 100
    ballspeedx = 5
    ballspeedy = 10
    tm = time.time()
    tr = trafficLight()
    global gameover, ball_coords_x, ball_coords_y
    init()
    while not gameover:
        handle_events()
        if (time.time() - tm > 0.000000003):
            ballrect.x += ballspeedx
            ballrect.y += ballspeedy
            if ballrect.x < 0 or ballrect.x + ballrect.width > width:
                ballspeedx *= -1
            if ballrect.y < 0 or ballrect.y + ballrect.height > height:
                ballspeedy *= -1
            screen.fill(bg)
            #tr.draw()
            tm = time.time()
        screen.blit(ball, ballrect)
        pygame.display.flip()



if __name__ == "__main__":
    main()
